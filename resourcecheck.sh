#!/bin/bash
clear
tput bold; cat ./header; tput sgr0
echo ""
echo "______________________________________________________________________________"
echo ""
tput bold; echo "This is a simple script that can identify resource usage spikes "
echo "Use the thresholds.cfg file to edit the thresholds"; tput sgr0
echo "______________________________________________________________________________"
echo ""
echo "Author: Ihsan Izwer                                              Version: 1.0"
echo "______________________________________________________________________________"
echo ""

memory=`free -m | grep Mem | tr -s "{ }" | cut -f4 -d' '`
storage=`df -h | grep /dev/*da | tr -s "{ }" | cut -f4 -d' ' | cut -f1 -d'G'`
cpu=`mpstat | grep all | tr -s "{ }" | cut -f13 -d' '| cut -f1 -d'.'`

while read config
do
	conflist+=($config)
	done < ./thresholds.cfg

if ([ $memory -le ${conflist[0]} ] && [ $storage -le ${conflist[1]} ] && [ $cpu -le ${conflist[2]} ]);then
	echo -e "Hi Ihsan\n, This is to inform you that there has been a resource usage spike. Please do the needful to resolve this issue./n Thanks" | mail -s "MaLeDiCry Resource Check: High CPU, RAM and Storage usage alert" m.ihsanizwer@gmail.com 
	echo "MaLeDiCry Resource Check: High CPU, RAM and Storage usage alert"

elif ([ $memory -le ${conflist[0]} ] && [ $storage -le ${conflist[1]} ]); then
	echo -e "Hi Ihsan\n, This is to inform you that there has been a resource usage spike. Please do the needful to resolve this issue./n Thanks" | mail -s "MaLeDiCry Resource Check: High RAM and Storage usage alert" m.ihsanizwer@gmail.com
	echo "MaLeDiCry Resource Check: High RAM and Storage usage alert"

elif ([ $memory -le ${conflist[0]} ] && [ $cpu -le ${conflist[2]} ]); then
	echo -e "Hi Ihsan\n, This is to inform you that there has been a resource usage spike. Please do the needful to resolve this issue./n Thanks" | mail -s "MaLeDiCry Resource Check: High CPU and RAM usage alert" m.ihsanizwer@gmail.com
	echo "MaLeDiCry Resource Check: High CPU and RAM usage alert"

elif ([ $storage -le ${conflist[1]} ] && [ $cpu -le ${conflist[2]} ]); then
	echo -e "Hi Ihsan\n, This is to inform you that there has been a resource usage spike. Please do the needful to resolve this issue./n Thanks" | mail -s "MaLeDiCry Resource Check: High CPU and Storage usage alert" m.ihsanizwer@gmail.com
	echo "MaLeDiCry Resource Check: High CPU and Storage usage alert"

elif [ $memory -le ${conflist[0]} ]; then
	echo -e "Hi Ihsan\n, This is to inform you that there has been a resource usage spike. Please do the needful to resolve this issue./n Thanks" | mail -s "MaLeDiCry Resource Check: High RAM usage alert" m.ihsanizwer@gmail.com
	echo "MaLeDiCry Resource Check: High RAM usage alert"

elif [ $storage -le ${conflist[1]} ]; then
	echo -e "Hi Ihsan\n, This is to inform you that there has been a resource usage spike. Please do the needful to resolve this issue./n Thanks" | mail -s "MaLeDiCry Resource Check: High Storage usage alert" m.ihsanizwer@gmail.com
	echo "MaLeDiCry Resource Check: High Storage usage alert"

elif [ $cpu -le ${conflist[2]} ]; then
	echo -e "Hi Ihsan\n, This is to inform you that there has been a resource usage spike. Please do the needful to resolve this issue./n Thanks" | mail -s "MaLeDiCry Resource Check: High CPU usage alert" m.ihsanizwer@gmail.com
	echo "MaLeDiCry Resource Check: High CPU usage alert"

else
	echo "MaLeDiCry Resource Check: Nothing to worry at this point :) "
	fi
	